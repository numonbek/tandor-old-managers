document.addEventListener('DOMContentLoaded', function () {
  initSwiper();
  initInputmask();
  initFormValidation();
  initFancyBox();
});
